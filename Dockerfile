# Menggunakan gambar resmi ASP.NET dari Microsoft
FROM mcr.microsoft.com/dotnet/aspnet:6.0

# Menyalin file aplikasi ASP.NET ke dalam kontainer
COPY ./bin/Release/net6.0/publish/ App/

# Menentukan direktori kerja
WORKDIR /App

# Mengaktifkan port yang akan digunakan oleh aplikasi
EXPOSE 80

# Menjalankan aplikasi ketika kontainer dimulai
CMD ["dotnet", "Client-Server"]
